#!/bin/bash

JOB_NUM=$1

for (( c=1; c<=$JOB_NUM; c++ )) do
  # Generate the file
  cat <<EOL
job$c:
  script: echo $c
EOL
done
